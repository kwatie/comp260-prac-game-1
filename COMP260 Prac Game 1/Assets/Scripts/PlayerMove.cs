﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour {

	public float maxSpeed = 5.0f;
	private BeeSpawner beeSpawner;
	public float destroyRadius = 1.0f;

	// Use this for initialization
	void Start () {
		beeSpawner = FindObjectOfType<BeeSpawner> ();
	
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetButton ("Fire1")) {
			//destroy a nearby bee
			beeSpawner.DestroyBees (
				transform.position, destroyRadius);
		}
		//get the input vlaues
		Vector2 direction;
		direction.x = Input.GetAxis ("Horizontal");
		direction.y = Input.GetAxis ("Vertical");

		//scale the velocity by the frame duration
		Vector2 velocity = direction * maxSpeed;

		//move the object
		transform.Translate(velocity * Time.deltaTime) ;

	}
}
